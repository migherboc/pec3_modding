using System.Collections.Generic;
using System.IO;
using UnityEngine;

public class LevelSaver : MonoBehaviour
{
    // Se almacena la posici�n y tipo de cada prefab en el nivel
    private List<string> posPrefabs = new List<string>();

    // Ruta de la carpeta donde se guardan los txt que contienen la informaci�n de los niveles
    private string directoryPath = "./niveles/";

    // Prefijo del nombre del txt donde se guarda la informaci�n del nivel.
    private string fileNamePrefix = "level";

    // Extensi�n del archivo txt donde se guarda la informaci�n del nivel.
    private string fileExtension = ".txt";

    // Posiciones excluidas al guardar el nivel (de forma que no aparezcan elementos no deseados).
    private Vector3[] posicionesExcluidas = {
        new Vector3(-7, 3, 0),
        new Vector3(-7, 1, 0),
        new Vector3(-7, -1, 0),
        new Vector3(-7, -3, 0)
    };

    // M�todo para guardar la posici�n y tipo de cada prefab en el nivel.
    public void SaveLevel()
    {
        // Se vacia la lista de posiciones.
        posPrefabs.Clear();

        // Se recorre toda la escena en busca de los objetos marcados.
        foreach (GameObject obj in GameObject.FindObjectsOfType<GameObject>())
        {
            // Si el objeto es un jugador, muro, caja o punto de victoria se guarda la posici�n y tipo del prefab en la lista de posiciones.
            if (obj.CompareTag("Player") || obj.CompareTag("Muro") || obj.CompareTag("Caja") || obj.CompareTag("PuntoVictoria"))
            {
                if (!EsPosExcluida(obj.transform.position))
                {
                    // Se guarda la posici�n y tipo del prefab en la lista de posiciones.
                    string objectPosition = obj.transform.position.x + "," + obj.transform.position.y + "," + obj.transform.position.z + "," + obj.tag;
                    posPrefabs.Add(objectPosition);
                }
            }
        }

        // Se obtiene el n�mero de archivos de texto en la carpeta de niveles.
        int fileNumber = Directory.GetFiles(directoryPath, fileNamePrefix + "*" + fileExtension).Length + 1;

        // Se crea el nombre del archivo de texto con el n�mero de nivel correspondiente.
        string fileName = directoryPath + fileNamePrefix + fileNumber + fileExtension;

        // Por si acaso, la carpeta de niveles se crea si no existe.
        if (!Directory.Exists(directoryPath))
        {
            Directory.CreateDirectory(directoryPath);
        }

        // Se crea un txt y se escribe en �l la lista de posiciones.
        using (StreamWriter writer = new StreamWriter(fileName))
        {
            foreach (string posicion in posPrefabs)
            {
                writer.WriteLine(posicion);
            }
        }
    }

    // Funci�n para no guardar los prefabs en las posiciones excluidas (prefabs usados para la creaci�n de niveles).
    private bool EsPosExcluida(Vector3 posicion)
    {
        foreach (Vector3 posicionExcluida in posicionesExcluidas)
        {
            if (posicion == posicionExcluida)
            {
                return true;
            }
        }
        return false;
    }
}

