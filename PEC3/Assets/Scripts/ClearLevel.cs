using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ClearLevel : MonoBehaviour
{
    // Funci�n para limpiar el editor y poder empezar a dise�ar un nuevo nivel de Sokoban.
    public void ClearObjects()
    {
        DraggableObject[] draggableObjects = FindObjectsOfType<DraggableObject>();
        foreach (DraggableObject draggableObject in draggableObjects)
        {
            // Antes de destruir los objetos, se verifica la posici�n de los objetos del editor, de forma que no desaparezcan.
            if (draggableObject.transform.position != new Vector3(-7, 3, 0) &&
                draggableObject.transform.position != new Vector3(-7, 1, 0) &&
                draggableObject.transform.position != new Vector3(-7, -1, 0) &&
                draggableObject.transform.position != new Vector3(-7, -3, 0))
            {
                Destroy(draggableObject.gameObject);
            }
        }
    }
}
