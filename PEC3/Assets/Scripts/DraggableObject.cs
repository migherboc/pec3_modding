using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DraggableObject : MonoBehaviour
{
    private bool isDragging = false;
    private Vector3 mouseOffset;
    private Vector3 posicionInicial;
    private GameObject clonPrefab;

    // Se guarda la posici�n inicial del objeto arrastrable.
    private void Start()
    {
        posicionInicial = transform.position;
    }

    // Se obtiene la posici�n del mouse y se calcula la diferencia entre la posici�n del objeto y la posici�n del rat�n. 
    void OnMouseDown()
    {
        mouseOffset = transform.position - GetMouseWorldPos();
        isDragging = true;
    }

    // Se actualiza la posici�n del objeto seg�n la posici�n del rat�n y la diferencia calculada en OnMouseDown().
    void OnMouseDrag()
    {
        if (isDragging)
        {
            transform.position = GetMouseWorldPos() + mouseOffset;
        }
    }

    // Se desactiva la variable isDragging y se redondea la posici�n del objeto a la cuadr�cula m�s cercana.Luego se crea un clon del objeto de forma que el editor no se quede sin prefabs.
    void OnMouseUp()
    {
        isDragging = false;
        transform.position = new Vector3(Mathf.Round(transform.position.x), Mathf.Round(transform.position.y), 0f);
        clonPrefab = Instantiate(gameObject, posicionInicial, Quaternion.identity);
        EstablecerEnCuadricula();
    }

    private Vector3 GetMouseWorldPos()
    {
        Vector3 mousePos = Input.mousePosition;
        mousePos.z = Camera.main.transform.position.z - transform.position.z;
        return Camera.main.ScreenToWorldPoint(mousePos);
    }

    // Se redondea la posici�n del objeto  a la cuadr�cula m�s cercana, de forma que el juego funcione correctamente.
    private void EstablecerEnCuadricula()
    {
        float gridSize = 1f;
        Vector3 newPos = transform.position;
        newPos.x = Mathf.Round(newPos.x / gridSize) * gridSize;
        newPos.y = Mathf.Round(newPos.y / gridSize) * gridSize;
        transform.position = newPos;
    }
}