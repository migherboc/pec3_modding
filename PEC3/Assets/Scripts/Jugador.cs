using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Jugador : MonoBehaviour
{
    // La funci�n Movimiento recibe un vector de direcci�n y mueve al jugador en esa direcci�n.
    public bool Movimiento(Vector2 direction)
    {
        // Se verifica si el movimiento es horizontal o vertical.
        if (Mathf.Abs(direction.x) < 0.5)
        {
            direction.x = 0;
        }

        else
        {
            direction.y = 0;
        }
        // Se normaliza la direcci�n en pos de asegurar que el jugador se mueva a una velocidad constante.
        direction.Normalize();

        // Se verifica si el jugador est� bloqueado en la direcci�n en la que se intenta moverse.
        if (Bloqueado(transform.position, direction))
        {
            return false;
        }
        else
        {
            // Si el jugador no est� bloqueado, se mueve en la direcci�n especificada.
            transform.Translate(direction);
            return true;
        }
        
    }

    // En esta funci�n se verifica si hay objetos que impidan el paso al jugador.
    bool Bloqueado(Vector3 position, Vector2 direction)
    {
        Vector2 posNueva = new Vector2(position.x, position.y) + direction;

        // Se buscan los GameObjects con el tag "Muro", estos objetos impiden el paso al jugador (se devuelve true).
        GameObject[] muros = GameObject.FindGameObjectsWithTag("Muro");
        foreach(var muro in muros)
        {
           if(muro.transform.position.x == posNueva.x && muro.transform.position.y == posNueva.y)
            {
                return true;
            }
        }

        // Se buscan los GameObjects con el tag "Caja", estos objetos pueden ser movidos por el jugador a no ser que esten bloqueados, se devuelve true o false dependiendo de este factor.
        GameObject[] cajas = GameObject.FindGameObjectsWithTag("Caja");
        foreach(var caja in cajas)
        {
            if(caja.transform.position.x == posNueva.x && caja.transform.position.y == posNueva.y)
            {
                CajaManager bx = caja.GetComponent<CajaManager>();
                if(bx && bx.Movimiento(direction))
                {
                    return false;
                }

                else
                {
                    return true;
                }
               
            }
        }
        return false;
        
    }
}
