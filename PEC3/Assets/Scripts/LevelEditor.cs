
using UnityEngine;
using UnityEngine.UI;
using System.Collections.Generic;

public class LevelEditor : MonoBehaviour
{
    public GameObject playerPrefab;
    public GameObject muroPrefab;
    public GameObject cajaPrefab;
    public GameObject prefab;
    public Vector3 posInicial;


    // Se instancia un nuevo prefab en la posici�n inicial
    public void SpawnPrefab(Vector3 position)
    {
        Instantiate(prefab, posInicial, Quaternion.identity);
    }

    private List<GameObject> spawnedObjects = new List<GameObject>();

    void Update()
    {
        if (Input.GetMouseButtonDown(0))
        {
            RaycastHit hit;
            if (Physics.Raycast(Camera.main.ScreenPointToRay(Input.mousePosition), out hit))
            {
                Vector3 position = new Vector3(Mathf.Round(hit.point.x), Mathf.Round(hit.point.y), Mathf.Round(hit.point.z));
                GameObject prefab = playerPrefab; // change this to the appropriate prefab based on user input
                GameObject spawnedObject = Instantiate(prefab, position, Quaternion.identity);
                spawnedObjects.Add(spawnedObject);
            }
        }
    }
}

