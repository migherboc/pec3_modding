using System.Collections.Generic;
using System.IO;
using UnityEngine;
using UnityEngine.SceneManagement;

public class LevelManager : MonoBehaviour
{
    // Enum representando todas las diferentes escenas donde se pueden cargar los niveles.
    public enum SceneType
    {
        Scene1,
        Scene2,
        Scene3,
        Scene4,
        Scene5,
        Scene6,
        Scene7,
        Scene8,
        Scene9,
        Scene10
    }


    // Ruta de la carpeta donde se encuentran los archivos txt que contienen la informaci�n de los niveles.
    public string directoryPath = "./niveles/";

    // Prefijo del nombre del archivo txt donde se encuentra la informaci�n del nivel.
    public string fileNamePrefix = "level";

    // Extensi�n del archivo de texto donde se encuentra la informaci�n del nivel.
    public string fileExtension = ".txt";

    // Escena donde se carga el nivel.
    public SceneType sceneType;

    // N�mero del nivel a cargar.
    public int numNivel = 1;

    // Prefabs para instanciar en el nivel
    public GameObject jugadorPrefab;
    public GameObject muroPrefab;
    public GameObject cajaPrefab;
    public GameObject puntoVictoriaPrefab;

    // M�todo para cargar el nivel
    public void LoadLevel(int numNivel)
    {
        // Actualizamos el n�mero de nivel
        this.numNivel = numNivel;

        // Obtenemos el nombre del archivo de texto con la informaci�n del nivel.
        string fileName = directoryPath + fileNamePrefix + numNivel + fileExtension;

        // Leemos el archivo de texto y obtenemos la lista de posiciones de los objetos en el nivel.
        List<string> objectPositions = new List<string>();
        using (StreamReader reader = new StreamReader(fileName))
        {
            while (!reader.EndOfStream)
            {
                objectPositions.Add(reader.ReadLine());
            }
        }

        // Instanciamos los objetos en las posiciones correspondientes.
        foreach (string position in objectPositions)
        {
            string[] positionData = position.Split(',');
            float x = float.Parse(positionData[0]);
            float y = float.Parse(positionData[1]);
            float z = float.Parse(positionData[2]);
            string tag = positionData[3];

            GameObject prefabToInstantiate = null;
            switch (tag)
            {
                case "Player":
                    prefabToInstantiate = jugadorPrefab;
                    break;
                case "Muro":
                    prefabToInstantiate = muroPrefab;
                    break;
                case "Caja":
                    prefabToInstantiate = cajaPrefab;
                    break;
                case "PuntoVictoria":
                    prefabToInstantiate = puntoVictoriaPrefab;
                    break;
                default:
                    break;
            }

            if (prefabToInstantiate != null)
            {
                Instantiate(prefabToInstantiate, new Vector3(x, y, z), Quaternion.identity);
            }
        }
    }

    // El nivel correcto se carga al empezar la escena.
    private void Start()
    {
        // Obtenemos el n�mero de nivel de la escena actual.
        int numNivel = 1;
        switch (sceneType)
        {
            case SceneType.Scene1:
                numNivel = 1;
                break;
            case SceneType.Scene2:
                numNivel = 2;
                break;
            case SceneType.Scene3:
                numNivel = 3;
                break;
            case SceneType.Scene4:
                numNivel = 4;
                break;
            case SceneType.Scene5:
                numNivel = 5;
                break;
            case SceneType.Scene6:
                numNivel = 6;
                break;
            case SceneType.Scene7:
                numNivel = 7;
                break;
            case SceneType.Scene8:
                numNivel = 8;
                break;
            case SceneType.Scene9:
                numNivel = 9;
                break;
            case SceneType.Scene10:
                numNivel = 10;
                break;
            default:
                break;
        }

        // Cargamos el nivel correspondiente
        LoadLevel(numNivel);

        // Desactivamos los BoxCollider2D de los objetos instanciados, de forma que no pueda haber problemas o bugs.
        GameObject[] objectsInScene = FindObjectsOfType<GameObject>();
        foreach (GameObject obj in objectsInScene)
        {
            BoxCollider2D boxCollider = obj.GetComponent<BoxCollider2D>();
            if (boxCollider != null)
            {
                boxCollider.enabled = false;
            }
        }
    }

    // Funci�n para comprobar que las cajas se encuentran en los recuadros verdes.
    public bool TodasCajasEnVictoria()
    {
        GameObject[] cajas = GameObject.FindGameObjectsWithTag("Caja");
        foreach (var caja in cajas)
        {
            CajaManager cajaManager = caja.GetComponent<CajaManager>();
            if (!cajaManager.encimaDePuntoVictoria)
            {
                return false;
            }
        }

        return true;
    }
}