using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GameManager : MonoBehaviour
{
    private bool permitirMov;
    private Jugador jugador;

    private void Start()
    {
        // Se busca el prefab con la etiqueta "Player" para obtener su componente jugador y que pueda controlarse.
        GameObject jugadorObj = GameObject.FindGameObjectWithTag("Player");
        if (jugadorObj != null)
        {
            jugador = jugadorObj.GetComponent<Jugador>();
        }
        
    }
    void Update()
    {
        // Alo largo de los niveles, la funci�n se sigue ejecutando para que no halla problemas para encontrar al jugador.
        GameObject jugadorObj = GameObject.FindGameObjectWithTag("Player");
        if (jugadorObj != null)
        {
            jugador = jugadorObj.GetComponent<Jugador>();
        }

        Vector2 controlMov = new Vector2(Input.GetAxisRaw("Horizontal"), Input.GetAxisRaw("Vertical"));
        controlMov.Normalize();
        if (controlMov.sqrMagnitude > 0.5)
        {
            if (permitirMov)
            {
                permitirMov = false;

                // Se verfica si el prefab del jugador todav�a existe antes de llamar a su funci�n de movimiento.
                if (jugador != null)
                {
                    jugador.Movimiento(controlMov);
                }
            }
        }
        else
        {
            permitirMov = true;
        }
    }
}