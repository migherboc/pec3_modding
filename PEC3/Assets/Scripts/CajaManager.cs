using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class CajaManager : MonoBehaviour
{
    public bool encimaDePuntoVictoria;

    private static List<CajaManager> cajasEnPuntoVictoria = new List<CajaManager>();
    private static List<GameObject> puntosVictoria = new List<GameObject>();

    private void Start()
    {
        // Al iniciar la escena, se resetean las listas.
        cajasEnPuntoVictoria.Clear();
        puntosVictoria.Clear();

        // Se buscan todas las cajas y puntos de victoria en la escena.
        GameObject[] cajas = GameObject.FindGameObjectsWithTag("Caja");
        GameObject[] puntos = GameObject.FindGameObjectsWithTag("PuntoVictoria");

        // Se agregan las cajas y los puntos de victoria a las listas correspondientes
        foreach (GameObject caja in cajas)
        {
            if (caja.GetComponent<CajaManager>() != null)
            {
                cajasEnPuntoVictoria.Add(caja.GetComponent<CajaManager>());
            }
        }

        foreach (GameObject punto in puntos)
        {
            puntosVictoria.Add(punto);
        }
    }

    // Funci�n de movimiento de las cajas (al ser empujadas).
    public bool Movimiento(Vector2 direction)
    {
        if (CajaBloqueada(transform.position, direction))
        {
            return false;
        }
        else
        {
            transform.Translate(direction);
            CheckVictoria();
            return true;
        }
    }

    // Funci�n que determina el comportamiento de las cajas al estar bloqueadas por muros u otras cajas.

    bool CajaBloqueada(Vector3 position, Vector2 direction)
    {
        Vector2 posNueva = new Vector2(position.x, position.y) + direction;
        GameObject[] muros = GameObject.FindGameObjectsWithTag("Muro");
        foreach (var muro in muros)
        {
            if (muro.transform.position.x == posNueva.x && muro.transform.position.y == posNueva.y)
            {
                return true;
            }
        }

        GameObject[] cajas = GameObject.FindGameObjectsWithTag("Caja");
        foreach (var caja in cajas)
        {
            if (caja.transform.position.x == posNueva.x && caja.transform.position.y == posNueva.y)
            {
                return true;
            }

        }
        return false;
    }

    // Funci�n para determinar que las cajas est�n encima de los puntos de victoria y que se pueda pasar a la siguiente escena.
    void CheckVictoria()
    {
        // Se buscan las cajas y puntos de victoria.
        GameObject[] cajas = GameObject.FindGameObjectsWithTag("Caja");
        GameObject[] puntosVictoria = GameObject.FindGameObjectsWithTag("PuntoVictoria");

        // Se crea una lista para almacenar las cajas que est�n sobre un punto de victoria.
        List<GameObject> cajasEnPuntosVictoria = new List<GameObject>();

        // Verificamos si todas las cajas en escena est�n sobre un punto de victoria.
        foreach (GameObject caja in cajas)
        {
            foreach (GameObject puntoVictoria in puntosVictoria)
            {
                if (caja.transform.position == puntoVictoria.transform.position)
                {
                    cajasEnPuntosVictoria.Add(caja);
                    break;
                }
            }
        }

        // Si todas las cajas est�n sobre puntos de victoria, se carga el siguiente nivel.
        if (cajasEnPuntosVictoria.Count == cajas.Length)
        {
            int nextSceneIndex = SceneManager.GetActiveScene().buildIndex + 1;
            if (nextSceneIndex < SceneManager.sceneCountInBuildSettings)
            {
                SceneManager.LoadScene(nextSceneIndex);
            }

            // Al acabar el �ltimo nivel se vuelve al primero.
            else
            {
                SceneManager.LoadScene(0); // Carga el nivel 1
            }
        }

        // Se cambia el color de las cajas a verde si est�n en un punto de victoria.
        foreach (GameObject caja in cajas)
        {
            bool cajaEnPuntoVictoria = cajasEnPuntosVictoria.Contains(caja);
            caja.GetComponent<SpriteRenderer>().color = cajaEnPuntoVictoria ? Color.green : Color.white;
        }
    }
}



