# PEC3_Modding

En esta PEC se ha creado un editor de niveles de Sokoban perfectamente funcional con el que se han diseñado los 10 niveles del juego.

El editor de niveles se encuentra en una escena aparte (no pudiendo ser jugada por el jugador). Se podrán crear los niveles de forma sencilla e intuitiva, atrastrando los componentes con el ratón, construyendo
el nivel con la forma deseada. Una vez se haya terminado el nivel, este se podrá guardar, y se creará en 
ese momento un archivo txt con la información del nivel. Acto seguido el nivel podrá ser cargado en cualquier
nivel de la escena, modificando los valores necesarios en el script y en el editor.

En el caso del juego, al ejecutar el mismo, el jugador podrá disfrutar de 10 niveles de Sokoban que irán 
aumentando de dificultad progresivamente. Al completar un nivel, se pasará automáticamente al siguiente, 
pudiendose resetar el nivel en cualquier momento mediante la pulsación de la tecla R. 

Como nota aparte, es notorio mencionar que el juego ha sido preparado para ser jugado en resolución 1080p,
mientras que el editor está preparado para ejecutarse en resolución libre (free aspect). Se ha diseñado así
en pos de trabajar de forma más efectiva, pero creo necesario mencionarlo en pos de que no haya problemas de
visualización. 

El videojuego se ha realizado con la versión de Unity 2021.3.18f1


- Enlaces a YouTube:

Gameplay de los 10 niveles de Sokoban: https://www.youtube.com/watch?v=sW5uFH8287I&ab_channel=MiguelHerr%C3%A1n

Demostración de la tool empleada: https://www.youtube.com/watch?v=WgjQIkN2J8c&ab_channel=MiguelHerr%C3%A1n